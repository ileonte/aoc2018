import sys

changes = []
sum = 0
sums = set()
first = None

sums.add(0)

def replay(deltas):
    global sum, sums, first
    for n in deltas:
        sum += n
        if not first:
            if sum in sums:
                first = sum
            else:
                sums.add(sum)

while True:
    try:
        n = int(input())
        changes.append(n)
    except:
        break

replay(changes)    
print('sum = {}'.format(sum))

while not first:
    replay(changes)
    print('sum = {} ({})'.format(sum, len(sums)))
print('first = {}'.format(first))
