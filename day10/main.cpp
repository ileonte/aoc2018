#include <vector>
#include <regex>
#include <optional>
#include <string>
#include <iostream>
#include <fmt/format.h>

#define pout(format, ...) fmt::print(::stdout, format "\n", ##__VA_ARGS__)
#define perr(format, ...) fmt::print(::stderr, format "\n", ##__VA_ARGS__)
#if defined(NDEBUG)
#define pdbg(format, ...)
#else
#define pdbg(format, ...) pout(format, ##__VA_ARGS__)
#endif

namespace fmt {
    template <typename T>
    struct formatter<std::vector<T>> {
        template <typename ParseContext>
        constexpr auto parse(ParseContext &ctx) { return ctx.begin(); }

        template <typename FormatContext>
        auto format(const std::vector<T> &v, FormatContext &ctx) {
            auto ret = format_to(ctx.begin(), "[");
            for (size_t i = 0; i < v.size(); i++)
                ret = format_to(ret, "{}{}", v[i], i < v.size() - 1 ? ", " : "");
            return format_to(ret, "]");
        }
    };
}

struct size {
    int w{0};
    int h{0};
};

struct velocity {
    int dx{0};
    int dy{0};

    static inline velocity abs_max() {
        return {
            std::numeric_limits<typeof(velocity::dx)>::max(),
            std::numeric_limits<typeof(velocity::dy)>::max()
        };
    }
    static inline velocity abs_min() {
        return {
            std::numeric_limits<typeof(velocity::dx)>::min(),
            std::numeric_limits<typeof(velocity::dy)>::min()
        };
    }

    velocity operator-() {
        return {-dx, -dy};
    }
};

struct point {
    int x{0};
    int y{0};

    static inline point abs_max() {
        return {
            std::numeric_limits<typeof(point::x)>::max(),
            std::numeric_limits<typeof(point::y)>::max()
        };
    }
    static inline point abs_min() {
        return {
            std::numeric_limits<typeof(point::x)>::min(),
            std::numeric_limits<typeof(point::y)>::min()
        };
    }

    inline point& operator+=(const velocity& v) {
        x += v.dx;
        y += v.dy;
        return *this;
    }

    inline point operator+(int i) const {
        return {x + i, y + i};
    }
    inline point operator-(int i) const {
        return {x - i, y - i};
    }

    inline point operator-(const point& other) const {
        return {x - other.x, y - other.y};
    }

    inline bool operator==(const point& other) const {
        return x == other.x && y == other.y;
    }
};

struct area {
    point top{};
    point bot{};

    inline std::vector<point> points_within(const std::vector<point>& points) const {
        std::vector<point> ret{};
        ret.reserve(points.size());
        for (const auto& p : points)
            if (contains_point(p)) ret.push_back(p);
        return ret;
    }

    inline void points_within(const std::vector<point>& points, std::vector<point>& ret) const {
        ret.clear();
        for (const auto& p : points)
            if (contains_point(p)) ret.push_back(p);
    }

    inline bool has_neighbours(const point& p, const std::vector<point>& points) const {
        area a{p - 1, p + 1};
        for (const auto& m : points) {
            if (p == m) continue;
            if (a.contains_point(m)) return true;
        }
        return false;
    }

    inline bool is_cluster(const std::vector<point>& points) const {
        std::vector<point> filtered_points{};

        filtered_points.reserve(points.size());
        points_within(points, filtered_points);

        for (const auto& p : filtered_points) {
            if (!has_neighbours(p, filtered_points))
                return false;
        }
        for (int col = top.x; col <= bot.x; col++) {
            auto it = std::find_if(filtered_points.begin(), filtered_points.end(), [&](const point& p) -> bool {
                return p.x == col;
            });
            if (it == filtered_points.end()) return false;
        }
        return true;
    }

    inline bool contains_point(const point& p) const {
        return p == point{std::clamp(p.x, top.x, bot.x), std::clamp(p.y, top.y, bot.y)};
    }

    inline bool contains_any(const std::vector<point>& points) const {
        auto it = std::find_if(points.begin(), points.end(), [this](const point& p) -> bool {
            return this->contains_point(p);
        });
        return it != points.end();
    }

    inline int width() const {
        return std::abs(bot.x - top.x) + 1;
    }
    inline int height() const {
        return std::abs(bot.y - top.y) + 1;
    }
    inline struct size size() const {
        return {width(), height()};
    }
};
using maybe_area = std::optional<area>;

struct rect {
    point top{};
    point bot{};

    static inline rect bounding_rect(const std::vector<point>& points) {
        assert(points.size() > 0);
        rect ret{points[0], points[0]};
        for (size_t i = 1; i < points.size(); i++) {
            ret.top.x = std::min(ret.top.x, points[i].x);
            ret.top.y = std::min(ret.top.y, points[i].y);
            ret.bot.x = std::max(ret.bot.x, points[i].x);
            ret.bot.y = std::max(ret.bot.y, points[i].y);
        }
        return ret;
    }

    inline bool is_cluster(const std::vector<point>& points) const {
        for (int line = top.y; line <= bot.y; line++) {
            auto it = std::find_if(points.begin(), points.end(), [&](const point& p) -> bool {
                return p.y == line;
            });
            if (it == points.end()) return false;
        }
        return true;
    }

    inline area vertical_slice(int dx, int width) const {
        point t{std::min(bot.x, top.x + dx), top.y};
        point b{std::min(bot.x, std::max(t.x, t.x + width - 1)), bot.y};
        return area{t, b};
    }

    inline maybe_area next_subcluster(int dx, int min_width, const std::vector<point>& points) const {
        maybe_area ret{};
        for (int w = min_width; w <= width(); w++) {
            area tmp = vertical_slice(dx, w);
            if (tmp.width() < w) break;
            if (tmp.is_cluster(points)) ret = tmp;
        }
        return ret;
    }

    inline maybe_area next_empty_area(int dx, int min_width, const std::vector<point>& points) const {
        area ret = vertical_slice(dx, min_width);
        if (ret.width() < min_width) return {};
        if (ret.contains_any(points)) return {};

        for (int w = min_width + 1; w <= width(); w++) {
            area tmp = vertical_slice(dx, w);
            if (tmp.contains_any(points)) break;
            ret = tmp;
        }

        return ret;
    }


    inline int width() const {
        return std::abs(bot.x - top.x) + 1;
    }
    inline int height() const {
        return std::abs(bot.y - top.y) + 1;
    }
    inline struct size size() const {
        return {width(), height()};
    }
};

namespace fmt {
    template <>
    struct formatter<struct size> {
        template <typename ParseContext>
        constexpr auto parse(ParseContext &ctx) { return ctx.begin(); }

        template <typename FormatContext>
        auto format(const struct size& v, FormatContext &ctx) {
            return format_to(ctx.begin(), "({}x{})", v.w, v.h);
        }
    };
    template <>
    struct formatter<point> {
        template <typename ParseContext>
        constexpr auto parse(ParseContext &ctx) { return ctx.begin(); }

        template <typename FormatContext>
        auto format(const point& v, FormatContext &ctx) {
            return format_to(ctx.begin(), "{{{}, {}}}", v.x, v.y);
        }
    };
    template <>
    struct formatter<velocity> {
        template <typename ParseContext>
        constexpr auto parse(ParseContext &ctx) { return ctx.begin(); }

        template <typename FormatContext>
        auto format(const velocity& v, FormatContext &ctx) {
            return format_to(ctx.begin(), "{{{}, {}}}", v.dx, v.dy);
        }
    };
    template <>
    struct formatter<area> {
        template <typename ParseContext>
        constexpr auto parse(ParseContext &ctx) { return ctx.begin(); }

        template <typename FormatContext>
        auto format(const area& v, FormatContext &ctx) {
            return format_to(ctx.begin(), "{{{} {}}}", v.top, v.bot);
        }
    };
    template <>
    struct formatter<rect> {
        template <typename ParseContext>
        constexpr auto parse(ParseContext &ctx) { return ctx.begin(); }

        template <typename FormatContext>
        auto format(const rect& v, FormatContext &ctx) {
            return format_to(ctx.begin(), "{{{} - {}}}", v.top, v.bot);
        }
    };
}

static inline void draw_state(const rect& bounding_rect, const std::vector<point>& points) {
    std::vector<char> state(bounding_rect.width() * bounding_rect.height(), ' ');
    for (const auto& p : points) {
        point coords = p - bounding_rect.top;
        state[coords.y * bounding_rect.width() + coords.x] = '#';
    }
    for (int y = 0; y < bounding_rect.height(); y++) {
        std::string_view row(state.data() + y * bounding_rect.width(), bounding_rect.width());
        pout("{}", row);
    }
}

static inline void simulation_step(ssize_t& current_time, rect& bounding_rect,
                                   std::vector<point>& points,
                                   const std::vector<velocity>& velocities) {
    current_time++;
    for (size_t i = 0; i < points.size(); i++)
        points[i] += velocities[i];
    bounding_rect = rect::bounding_rect(points);
}

/*
 * The following conditions are checked by this heuristic:
 *   - bounding box height is at least 5 "pixels"
 *   - two or more glyphs (sub-clusters) are present in the input
 *   - the minimum width for a valid glyph is 3 "pixels"
 *   - the minimum spacing between consecutive glyphs is 1 "pixel"
 */
static inline bool test_glyph_pattern(const rect& bounding_rect, const std::vector<point>& points) {
    static constexpr const int min_glyph_width = 3;
    static constexpr const int min_space_width = 1;
    static constexpr const int min_glyph_height = 5;

    int current_x{0};
    std::vector<int> possible_glyphs{};
    std::vector<int> spacings{};

    if (bounding_rect.height() < min_glyph_height) return false;

    auto area = bounding_rect.next_subcluster(current_x, min_glyph_width, points);
    if (!area.has_value()) return false;
    if (area->width() >= bounding_rect.width()) return false;
    possible_glyphs.push_back(area->width());
    current_x += area->width();

    auto space = bounding_rect.next_empty_area(current_x, min_space_width, points);
    if (!space.has_value()) return false;
    spacings.push_back(space->width());
    current_x += space->width();

    while (current_x < bounding_rect.width()) {
        area = bounding_rect.next_subcluster(current_x, min_glyph_width, points);
        if (!area.has_value()) return false;
        possible_glyphs.push_back(area->width());
        current_x += area->width();

        if (current_x == bounding_rect.width()) break;

        space = bounding_rect.next_empty_area(current_x, min_space_width, points);
        if (!space.has_value()) return false;
        current_x += space->width();
        spacings.push_back(space->width());
    }

    pout("FINE GRAINED CHECK found {} possible glyphs:\n  - widths: {}\n  - spacings: {}",
         possible_glyphs.size(),
         possible_glyphs,
         spacings);

    return true;
}

int main() {
    std::vector<point> points{};
    std::vector<velocity> velocities{};
    rect bounding_rect{};
    ssize_t current_time{-1};

    std::string line{};
    while (std::getline(std::cin, line)) {
        static const std::regex re_in("\\s*position=<\\s*(-?\\d+),\\s*(-?\\d+)> velocity=<\\s*(-?\\d+),\\s*(-?\\d+)>");
        std::smatch m{};

        if (!std::regex_match(line, m, re_in)) {
            perr("Failed to parse line '{}'", line);
            return 1;
        }

        velocities.push_back({std::stoi(m[3].str()), std::stoi(m[4].str())});
        points.push_back({std::stoi(m[1].str()), std::stoi(m[2].str())});

        points.back() += -velocities.back();
    }

    bool coarse_check{false};
    int coarse_count{0};
    int fine_count{0};

    while (true) {
        simulation_step(current_time, bounding_rect, points, velocities);

        coarse_check = bounding_rect.is_cluster(points);
        if (coarse_check) break;
    }

    while (coarse_check) {
        coarse_count++;

        bool fine_check = test_glyph_pattern(bounding_rect, points);
        pout("time = {}, bounding box check: {:^11} {} => {} by fine-grained check",
             current_time,
             fmt::format("{}", bounding_rect.size()), bounding_rect,
             fine_check ? "ACCEPTED" : "REJECTED");

        if (fine_check) {
            fine_count++;
            draw_state(bounding_rect, points);
        }

        simulation_step(current_time, bounding_rect, points, velocities);
        coarse_check = bounding_rect.is_cluster(points);
    }

    pout("Checked states: {} selected via coarse filter, {} accepted by fine-grained filter", coarse_count, fine_count);

    return 0;
}
