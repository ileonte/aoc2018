#include <vector>
#include <array>
#include <string>
#include <unordered_map>
#include <regex>
#include <optional>
#include <ctime>

#include <iostream>
#include <fmt/format.h>
#include <fmt/ostream.h>

#define pout(format, ...) fmt::print(::stdout, format "\n", ##__VA_ARGS__)
#define perr(format, ...) fmt::print(::stderr, format "\n", ##__VA_ARGS__)

namespace fmt {
    template <typename T>
    struct formatter<std::vector<T>> {
        template <typename ParseContext>
        constexpr auto parse(ParseContext &ctx) { return ctx.begin(); }

        template <typename FormatContext>
        auto format(const std::vector<T> &v, FormatContext &ctx) {
            auto ret = format_to(ctx.begin(), "[");
            for (size_t i = 0; i < v.size(); i++)
                ret = format_to(ret, "{}{}", v[i], i < v.size() - 1 ? ", " : "");
            return format_to(ret, "]");
        }
    };

    template <typename T, size_t N>
    struct formatter<std::array<T, N>> {
        template <typename ParseContext>
        constexpr auto parse(ParseContext& ctx) { return ctx.begin(); }

        template <typename FormatContext>
        auto format(const std::array<T, N>& arr, FormatContext& ctx) {
            auto ret = format_to(ctx.begin(), "[");
            for (size_t i = 0; i < arr.size(); i++)
                ret = format_to(ret, "{}{}", arr[i], i < arr.size() - 1 ? ", " : "");
            return format_to(ret, "]");
        }
    };
}

enum class event_type {
    begin,
    sleep,
    awake,
};
inline std::ostream& operator<<(std::ostream& out, event_type type) {
    switch (type) {
        case event_type::begin: return out << "EVT::BEGIN";
        case event_type::sleep: return out << "EVT::SLEEP";
        case event_type::awake: return out << "EVT::AWAKE";
    }
    return out;
}

struct event {
    event_type type{event_type::begin};
    int guard_id{-1};
    std::tm time{};

    using maybe = std::optional<event>;
    static maybe parse_line(const std::string& line) {
        static const std::regex re_begin("\\[([^\\]]+)\\] Guard #(\\d+) begins shift");
        static const std::regex re_sleep("\\[([^\\]]+)\\] falls asleep");
        static const std::regex re_awake("\\[([^\\]]+)\\] wakes up");
        std::smatch m{};
        event ret{};

        ::memset(&ret.time, 0, sizeof(ret.time));
        if (std::regex_match(line, m, re_begin)) {
            ::strptime(m[1].str().c_str(), "%Y-%m-%d %H:%M", &ret.time);
            ret.type = event_type::begin;
            ret.guard_id = std::stoi(m[2].str());
            return ret;
        }
        if (std::regex_match(line, m, re_sleep)) {
            ::strptime(m[1].str().c_str(), "%Y-%m-%d %H:%M", &ret.time);
            ret.type = event_type::sleep;
            return ret;
        }
        if (std::regex_match(line, m, re_awake)) {
            ::strptime(m[1].str().c_str(), "%Y-%m-%d %H:%M", &ret.time);
            ret.type = event_type::awake;
            return ret;
        }

        return {};
    }
};
namespace fmt {
    template <>
    struct formatter<event> {
        template <typename ParseContext>
        constexpr auto parse(ParseContext &ctx) { return ctx.begin(); }

        template <typename FormatContext>
        auto format(const event& ev, FormatContext &ctx) {
            char buff[128] = {};
            ::strftime(buff, sizeof(buff), "%Y-%m-%d %H:%M", &ev.time);
            return format_to(ctx.begin(),
                             "{{{}, time = {}, guard_id = {}}}",
                             ev.type, buff, ev.guard_id);
        }
    };
}
inline bool operator<(const event& e1, const event& e2) {
    return std::mktime(const_cast<std::tm*>(&e1.time)) < std::mktime(const_cast<std::tm*>(&e2.time));
}

struct guard_data {
    using maybe = std::optional<guard_data>;

    int id{-1};
    int minutes_asleep{0};
    std::array<int, 60> minute_map{};
    int last_sleep_minute{};

    void process_event(const event& ev) {
        switch (ev.type) {
            case event_type::begin: {
                assert(id == -1 || id == ev.guard_id);
                id = ev.guard_id;
                break;
            }
            case event_type::sleep: {
                last_sleep_minute = ev.time.tm_min;
                break;
            }
            case event_type::awake: {
                for (int i = last_sleep_minute; i < ev.time.tm_min; i++)
                    minute_map[i] += 1;
                minutes_asleep += (ev.time.tm_min - last_sleep_minute);
                break;
            }
        }
    }
};
namespace fmt {
    template <>
    struct formatter<guard_data> {
        template <typename ParseContext>
        constexpr auto parse(ParseContext &ctx) { return ctx.begin(); }

        template <typename FormatContext>
        auto format(const guard_data& guard, FormatContext &ctx) {
            return format_to(ctx.begin(),
                             "{{\n   id = {},\n   asleep = {},\n   map = {}\n}}",
                             guard.id, guard.minutes_asleep, guard.minute_map);
        }
    };
}

static std::vector<event> events;
static std::unordered_map<int, guard_data> guards;

static inline void part1() {
    guard_data* last_guard{nullptr};
    for (auto& [k, v] : guards) {
        if (!last_guard) last_guard = &v;
        else {
            if (last_guard->minutes_asleep < v.minutes_asleep) last_guard = &v;
        }
    }
    assert(last_guard);

    size_t m = 0;
    int r{0};
    int last_max{-1};
    for (m = 0; m < last_guard->minute_map.size(); m++) {
        if (last_max < last_guard->minute_map[m]) {
            r = m;
            last_max = last_guard->minute_map[m];
        }
    }
    pout("GUARD = {} ({} {})", *last_guard, last_guard->id, r);
    pout("PART 1: {}", last_guard->id * r);
}

static inline void part2() {
    std::array<int, 60> ids{};
    std::array<int, 60> counts{};
    size_t max_m{0};
    int last_max{-1};

    for (const auto& [k, g] : guards) {
        for (size_t m = 0; m < 60; m++) {
            if (counts[m] < g.minute_map[m]) {
                counts[m] = g.minute_map[m];
                ids[m] = g.id;
            }

            if (last_max < g.minute_map[m]) {
                last_max = g.minute_map[m];
                max_m = m;
            }
        }
    }

    const auto& guard = guards[ids[max_m]];
    pout("PART 2: {}", guard.id * max_m);
}

int main() {
    for (std::string line; std::getline(std::cin, line);) {
        if (auto r = event::parse_line(line); !r) {
            perr("Failed to parse line '{}'", line);
            continue;
        } else {
            events.emplace_back(std::move(r.value()));
        }
    }
    std::sort(events.begin(), events.end());

    guard_data* last_guard{nullptr};
    for (const auto& ev : events) {
        switch (ev.type) {
            case event_type::begin: {
                auto [it, _] = guards.try_emplace(ev.guard_id);
                last_guard = &it->second;
                [[fallthrough]];
            }
            default: {
                last_guard->process_event(ev);
                break;
            }
        }
    }

    part1();
    part2();

    return 0;
}
