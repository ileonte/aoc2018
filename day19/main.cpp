#include <set>
#include <vector>
#include <array>
#include <list>
#include <regex>
#include <optional>
#include <string>
#include <iostream>
#include <fstream>
#include <fmt/format.h>
#include <fmt/ostream.h>

#define pout(format, ...) fmt::print(::stdout, format "\n", ##__VA_ARGS__)
#define perr(format, ...) fmt::print(::stderr, format "\n", ##__VA_ARGS__)
#if defined(NDEBUG)
#define pdbg(format, ...)
#else
#define pdbg(format, ...) pout(format, ##__VA_ARGS__)
#endif

namespace fmt {
    template <typename T>
    struct formatter<std::vector<T>> {
        template <typename ParseContext>
        constexpr auto parse(ParseContext &ctx) { return ctx.begin(); }

        template <typename FormatContext>
        auto format(const std::vector<T> &v, FormatContext &ctx) {
            auto ret = format_to(ctx.begin(), "[");
            for (size_t i = 0; i < v.size(); i++)
                ret = format_to(ret, "{}{}", v[i], i < v.size() - 1 ? ", " : "");
            return format_to(ret, "]");
        }
    };

    template <typename T, size_t N>
    struct formatter<std::array<T, N>> {
        template <typename ParseContext>
        constexpr auto parse(ParseContext& ctx) { return ctx.begin(); }

        template <typename FormatContext>
        auto format(const std::array<T, N>& arr, FormatContext& ctx) {
            auto ret = format_to(ctx.begin(), "[");
            for (size_t i = 0; i < arr.size(); i++)
                ret = format_to(ret, "{}{}", arr[i], i < arr.size() - 1 ? ", " : "");
            return format_to(ret, "]");
        }
    };
}

static constexpr const size_t reg_count = 6;
using regpack = std::array<int, reg_count>;

struct instruction {
    int code{0};
    int A{0};
    int B{0};
    int C{0};
};
namespace fmt {
    template <>
    struct formatter<instruction> {
        template <typename ParseContext>
        constexpr auto parse(ParseContext &ctx) { return ctx.begin(); }
        template <typename FormatContext>
        auto format(const instruction& v, FormatContext &ctx) {
            return format_to(ctx.begin(), "{:>2} {:>2} {:>2} {:>2}", v.code, v.A, v.B, v.C);
        }
    };
}

using instruction_proc = void(*)(const instruction&, regpack&);
struct instruction_desc {
    const char* name{};
    instruction_proc proc;
};
static constexpr std::array<instruction_desc, 16> instructions {{
    {"addr", [](const instruction& instr, regpack& regs) { regs[instr.C] = regs[instr.A] + regs[instr.B]; } },
    {"addi", [](const instruction& instr, regpack& regs) { regs[instr.C] = regs[instr.A] + instr.B; } },

    {"mulr", [](const instruction& instr, regpack& regs) { regs[instr.C] = regs[instr.A] * regs[instr.B]; } },
    {"muli", [](const instruction& instr, regpack& regs) { regs[instr.C] = regs[instr.A] * instr.B; } },

    {"banr", [](const instruction& instr, regpack& regs) { regs[instr.C] = regs[instr.A] & regs[instr.B]; } },
    {"bani", [](const instruction& instr, regpack& regs) { regs[instr.C] = regs[instr.A] & instr.B; } },

    {"borr", [](const instruction& instr, regpack& regs) { regs[instr.C] = regs[instr.A] | regs[instr.B]; } },
    {"bori", [](const instruction& instr, regpack& regs) { regs[instr.C] = regs[instr.A] | instr.B; } },

    {"setr", [](const instruction& instr, regpack& regs) { regs[instr.C] = regs[instr.A]; } },
    {"seti", [](const instruction& instr, regpack& regs) { regs[instr.C] = instr.A; } },

    {"gtir", [](const instruction& instr, regpack& regs) { regs[instr.C] = (instr.A > regs[instr.B]); } },
    {"gtri", [](const instruction& instr, regpack& regs) { regs[instr.C] = (regs[instr.A] > instr.B); } },
    {"gtrr", [](const instruction& instr, regpack& regs) { regs[instr.C] = (regs[instr.A] > regs[instr.B]); } },

    {"eqir", [](const instruction& instr, regpack& regs) { regs[instr.C] = (instr.A == regs[instr.B]); } },
    {"eqri", [](const instruction& instr, regpack& regs) { regs[instr.C] = (regs[instr.A] == instr.B); } },
    {"eqrr", [](const instruction& instr, regpack& regs) { regs[instr.C] = (regs[instr.A] == regs[instr.B]); } }
}};

struct state_t {
    int ip_reg{0};
    int ip{0};
    regpack regs{};
};
namespace fmt {
    template <>
    struct formatter<state_t> {
        template <typename ParseContext>
        constexpr auto parse(ParseContext &ctx) { return ctx.begin(); }
        template <typename FormatContext>
        auto format(const state_t& v, FormatContext &ctx) {
            return format_to(ctx.begin(), "{{ip_reg = {}, ip = {:>2}, regs = {}}}", v.ip_reg, v.ip, v.regs);
        }
    };
}

static std::vector<instruction> program{};
static state_t state{};

static inline bool read_input(std::istream& in) {
    std::string line{};
    while (std::getline(in, line)) {
        static const std::regex re_bindip("#ip\\s+(\\d+)");
        static const std::regex re_instr("([a-z]+)\\s+(\\d+)\\s+(\\d+)\\s+(\\d+)");
        std::smatch m{};

        if (std::regex_match(line, m, re_bindip)) {
            state.ip_reg = std::stoi(m[1].str());
            continue;
        }

        if (std::regex_match(line, m, re_instr)) {
            const auto desc_it = std::find_if(instructions.begin(), instructions.end(), [&](const instruction_desc& d) {
                return m[1].str() == d.name;
            });
            if (desc_it == instructions.end()) {
                perr("Unknown instrunction '{}'", m[1].str());
                return false;
            }
            const auto& desc = *desc_it;
            program.push_back({int(&desc - instructions.data()), std::stoi(m[2].str()), std::stoi(m[3].str()), std::stoi(m[4].str())});
            continue;
        }

        perr("Failed to parse input line '{}'", line);
        return false;
    }
    return true;
}

static inline bool step_one(state_t& state) {
    if (std::clamp(state.ip, 0, (int)program.size() - 1) != state.ip) return false;
    state.regs[state.ip_reg] = state.ip;
    const auto& instr = program[state.ip];
    const auto& desc = instructions[instr.code];
    desc.proc(instr, state.regs);
    state.ip = state.regs[state.ip_reg] + 1;
    return true;
}

static inline void part1() {
    state_t tmp = state;
    while (step_one(tmp)) {}
    pout("PART 1: {}", tmp.regs[0]);
}

static inline void part2() {
    state_t tmp = state;
    tmp.regs[0] = 1;
    while (step_one(tmp)) {}
    pout("PART 2: {}", tmp.regs[0]);
}

int main(int argc, char* argv[]) {
    if (argc >= 2) {
        std::ifstream ifs(argv[1]);
        if (!ifs.is_open()) {
            perr("Failed to open file '{}'", argv[1]);
            return 1;
        }
        if (!read_input(ifs)) return 1;
    } else {
        if (!read_input(std::cin)) return 1;
    }

    part1();
    part2();

    return 0;
}
