#include <vector>
#include <regex>
#include <optional>
#include <string>
#include <iostream>
#include <fmt/format.h>

#define pout(format, ...) fmt::print(::stdout, format "\n", ##__VA_ARGS__)
#define perr(format, ...) fmt::print(::stderr, format "\n", ##__VA_ARGS__)
#if defined(NDEBUG)
#define pdbg(format, ...)
#else
#define pdbg(format, ...) pout(format, ##__VA_ARGS__)
#endif

static constexpr size_t const alphabet_size = 'Z' - 'A' + 1;

namespace fmt {
    template <typename T>
    struct formatter<std::vector<T>> {
        template <typename ParseContext>
        constexpr auto parse(ParseContext &ctx) { return ctx.begin(); }

        template <typename FormatContext>
        auto format(const std::vector<T> &v, FormatContext &ctx) {
            auto ret = format_to(ctx.begin(), "[");
            for (size_t i = 0; i < v.size(); i++)
                ret = format_to(ret, "{}{}", v[i], i < v.size() - 1 ? ", " : "");
            return format_to(ret, "]");
        }
    };
}

struct step {
    int id{-1};
    std::vector<char> prereqs{};
    bool done{false};

    using maybe = std::optional<std::pair<char, char>>;
    static inline maybe parse(const std::string& line) {
        static std::regex const re_step("Step ([^\\s]+) must be finished before step ([^\\s]+) can begin.");
        std::smatch m{};

        if (std::regex_match(line, m, re_step))
            return {{m[2].str().front(), m[1].str().front()}};

        return {};
    }

    static inline int name_to_id(char c) {
        return ((c >= 'A' && c <= 'Z') ? c - 'A' + 1 : -1);
    }
    static inline char id_to_name(int i) {
        return ((i >= 1 && i <= (int)alphabet_size) ? i - 1 + 'A' : 0);
    }

    inline char name() const {
        return id_to_name(id);
    }

    inline bool has_prereq(char p) const {
        return std::find(prereqs.begin(), prereqs.end(), p) != prereqs.end();
    }

    inline void fulfill_prereq(char p) {
        auto it = std::find(prereqs.begin(), prereqs.end(), p);
        if (it != prereqs.end()) *it = ' ';
    }

    inline size_t unmet_prereqs() const {
        return std::count_if(prereqs.begin(), prereqs.end(), [](const char& p) -> bool {
            return p != ' ';
        });
    }
};
static inline bool operator==(const step& s, char n) {
    return s.name() == n;
}
namespace fmt {
    template <>
    struct formatter<step> {
        template <typename ParseContext>
        constexpr auto parse(ParseContext &ctx) { return ctx.begin(); }

        template <typename FormatContext>
        auto format(const step& s, FormatContext &ctx) {
            return format_to(ctx.begin(), "{{name: '{}' ({:2d}), done : {:<5}, prereqs: {} ({} {})}}",
                             s.name(), s.id,
                             s.done,
                             s.prereqs, s.prereqs.size(), s.unmet_prereqs());
        }
    };
}

using step_list = std::vector<step>;
static step_list input{};

static inline auto ensure_step_exists(step_list& steps, char name) {
    auto it = std::find(steps.begin(), steps.end(), name);
    if (it == steps.end()) {
        it = steps.emplace(steps.end());
        (*it).id = step::name_to_id(name);
        (*it).prereqs.reserve(alphabet_size);
    }
    return it;
}

static inline const step* first_available_step(const step_list& steps) {
    const step* ret{nullptr};
    for (const auto& s : steps) {
        if (s.done) continue;
        if (s.unmet_prereqs() > 0) continue;

        if (!ret) {
            ret = &s;
            continue;
        }

        if (s.name() < ret->name()) {
            ret = &s;
            continue;
        }
    }
    return ret;
}

static inline void execute_step(step_list& steps, char name) {
    auto it = ensure_step_exists(steps, name);
    (*it).done = true;
    for (auto& s : steps)
        s.fulfill_prereq(name);
}

static inline void part1() {
    step_list steps(input);
    std::string ret{};
    ret.reserve(input.size());

    auto next = first_available_step(steps);
    while (next) {
        ret.push_back(next->name());
        execute_step(steps, next->name());

        next = first_available_step(steps);
    }

    pout("PART 1: {}", ret);
}

class worker_manager {
public:
    struct info {
        char name;
        size_t duration;
    };

    inline worker_manager(size_t count) : slots_(count, {' ', 0})
    {}

    inline bool executing(char name) const {
        for (const auto& worker : slots_)
            if (worker.name == name) return true;
        return false;
    }

    inline bool begin_execution(char name, size_t duration) {
        if (executing(name)) return false;
        for (auto& worker : slots_) {
            if (worker.duration) continue;

            worker.name = name;
            worker.duration = duration;
            return true;
        }
        return false;
    }

    inline void tick() {
        for (auto& worker : slots_)
            if (worker.duration > 0) worker.duration -= 1;
    }

    inline bool done() const {
        for (const auto& worker : slots_)
            if (worker.duration > 0) return false;
        return true;
    }

    inline const auto& slots() const {
        return slots_;
    }

    inline void mark_done(char name) {
        for (auto& worker : slots_) {
            if (worker.name == name) {
                worker.name = ' ';
                worker.duration = 0;
                return;
            }
        }
    }

private:
    std::vector<info> slots_{};
};
namespace fmt {
    template <>
    struct formatter<worker_manager::info> {
        template <typename ParseContext>
        constexpr auto parse(ParseContext &ctx) { return ctx.begin(); }

        template <typename FormatContext>
        auto format(const worker_manager::info& i, FormatContext &ctx) {
            return format_to(ctx.begin(), "{{'{}', {:>2}}}", i.name, i.duration);
        }
    };
    template <>
    struct formatter<worker_manager> {
        template <typename ParseContext>
        constexpr auto parse(ParseContext &ctx) { return ctx.begin(); }

        template <typename FormatContext>
        auto format(const worker_manager& w, FormatContext &ctx) {
            return format_to(ctx.begin(), "{{count: {}, slots: {}}}", w.slots().size(), w.slots());
        }
    };
}

static inline auto next_chunk(const step_list& steps) {
    std::vector<char> ret{};
    ret.reserve(alphabet_size);
    for (const auto& s : steps) {
        if (s.done) continue;
        if (!s.unmet_prereqs()) ret.push_back(s.name());
    }
    std::sort(ret.begin(), ret.end());
    return ret;
}

static inline void part2() {
    step_list steps(input);
    std::vector<size_t> task_durations(alphabet_size, 0);
    size_t ret{0};
    worker_manager workers(input.size() == 6 ? 2 : 5);

    for (const auto& step : steps) {
        size_t idx = step.id - 1;
        task_durations[idx] = step.id + (steps.size() == 6 ? 0 : 60);
    }
    pdbg("{}", task_durations);

    while (true) {
        workers.tick();
        for (const auto& worker : workers.slots()) {
            if (worker.name != ' ' && !worker.duration) {
                pdbg("TASK DONE: '{}'", worker.name);
                execute_step(steps, worker.name);
                workers.mark_done(worker.name);
            }
        }

        auto chunk = next_chunk(steps);
        if (chunk.empty()) {
            while (!workers.done()) {
                workers.tick();
                ret++;
            }
            break;
        } else {
            for (auto name : chunk)
                workers.begin_execution(name, task_durations[name - 'A']);
        }
        pdbg("{:>4d} -> {}, CHUNK: {}", ret, workers, chunk);
        ret++;
    }

    pout("PART 2: {}", ret);
}

int main() {
    std::string line{};

    input.reserve(alphabet_size);
    while (std::getline(std::cin, line)) {
        auto r = step::parse(line);
        if (!r) {
            perr("Failed to parse '{}'", line);
            return 1;
        }

        ensure_step_exists(input, r.value().first)->prereqs.push_back(r.value().second);
        ensure_step_exists(input, r.value().second);
    }

#if !defined(NDEBUG)
    for (const auto& step : input)
        pdbg("{}", step);
#endif

    part1();
    part2();

    return 0;
}
