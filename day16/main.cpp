#include <set>
#include <vector>
#include <array>
#include <list>
#include <regex>
#include <optional>
#include <string>
#include <iostream>
#include <fstream>
#include <fmt/format.h>
#include <fmt/ostream.h>

#define pout(format, ...) fmt::print(::stdout, format "\n", ##__VA_ARGS__)
#define perr(format, ...) fmt::print(::stderr, format "\n", ##__VA_ARGS__)
#if defined(NDEBUG)
#define pdbg(format, ...)
#else
#define pdbg(format, ...) pout(format, ##__VA_ARGS__)
#endif

namespace fmt {
    template <typename T>
    struct formatter<std::vector<T>> {
        template <typename ParseContext>
        constexpr auto parse(ParseContext &ctx) { return ctx.begin(); }

        template <typename FormatContext>
        auto format(const std::vector<T> &v, FormatContext &ctx) {
            auto ret = format_to(ctx.begin(), "[");
            for (size_t i = 0; i < v.size(); i++)
                ret = format_to(ret, "{}{}", v[i], i < v.size() - 1 ? ", " : "");
            return format_to(ret, "]");
        }
    };

    template <typename T, size_t N>
    struct formatter<std::array<T, N>> {
        template <typename ParseContext>
        constexpr auto parse(ParseContext& ctx) { return ctx.begin(); }

        template <typename FormatContext>
        auto format(const std::array<T, N>& arr, FormatContext& ctx) {
            auto ret = format_to(ctx.begin(), "[");
            for (size_t i = 0; i < arr.size(); i++)
                ret = format_to(ret, "{}{}", arr[i], i < arr.size() - 1 ? ", " : "");
            return format_to(ret, "]");
        }
    };
}

static constexpr const size_t reg_count = 4;
using regpack = std::array<int, reg_count>;

struct instruction {
    int code{0};
    int A{0};
    int B{0};
    int C{0};
};
namespace fmt {
    template <>
    struct formatter<instruction> {
        template <typename ParseContext>
        constexpr auto parse(ParseContext &ctx) { return ctx.begin(); }
        template <typename FormatContext>
        auto format(const instruction& v, FormatContext &ctx) {
            return format_to(ctx.begin(), "{:>2} {:>2} {:>2} {:>2}", v.code, v.A, v.B, v.C);
        }
    };
}


using instruction_proc = void(*)(const instruction&, regpack&);

struct instruction_desc {
    const char* name{};
    instruction_proc proc;
};
static constexpr std::array<instruction_desc, 16> instructions {{
    {"addr", [](const instruction& instr, regpack& regs) { regs[instr.C] = regs[instr.A] + regs[instr.B]; } },
    {"addi", [](const instruction& instr, regpack& regs) { regs[instr.C] = regs[instr.A] + instr.B; } },

    {"mulr", [](const instruction& instr, regpack& regs) { regs[instr.C] = regs[instr.A] * regs[instr.B]; } },
    {"muli", [](const instruction& instr, regpack& regs) { regs[instr.C] = regs[instr.A] * instr.B; } },

    {"banr", [](const instruction& instr, regpack& regs) { regs[instr.C] = regs[instr.A] & regs[instr.B]; } },
    {"bani", [](const instruction& instr, regpack& regs) { regs[instr.C] = regs[instr.A] & instr.B; } },

    {"borr", [](const instruction& instr, regpack& regs) { regs[instr.C] = regs[instr.A] | regs[instr.B]; } },
    {"bori", [](const instruction& instr, regpack& regs) { regs[instr.C] = regs[instr.A] | instr.B; } },

    {"setr", [](const instruction& instr, regpack& regs) { regs[instr.C] = regs[instr.A]; } },
    {"seti", [](const instruction& instr, regpack& regs) { regs[instr.C] = instr.A; } },

    {"gtir", [](const instruction& instr, regpack& regs) { regs[instr.C] = (instr.A > regs[instr.B]); } },
    {"gtri", [](const instruction& instr, regpack& regs) { regs[instr.C] = (regs[instr.A] > instr.B); } },
    {"gtrr", [](const instruction& instr, regpack& regs) { regs[instr.C] = (regs[instr.A] > regs[instr.B]); } },

    {"eqir", [](const instruction& instr, regpack& regs) { regs[instr.C] = (instr.A == regs[instr.B]); } },
    {"eqri", [](const instruction& instr, regpack& regs) { regs[instr.C] = (regs[instr.A] == instr.B); } },
    {"eqrr", [](const instruction& instr, regpack& regs) { regs[instr.C] = (regs[instr.A] == regs[instr.B]); } }
}};
static std::array<const instruction_desc*, instructions.size()> resolved_instructions{};

struct p1_input_t {
    regpack before{};
    instruction instr{};
    regpack after{};
};
namespace fmt {
    template <>
    struct formatter<p1_input_t> {
        template <typename ParseContext>
        constexpr auto parse(ParseContext &ctx) { return ctx.begin(); }
        template <typename FormatContext>
        auto format(const p1_input_t& v, FormatContext &ctx) {
            return format_to(ctx.begin(), "{{before: {}, instr: {}, after: {}}}",
                             v.before, v.instr, v.after);
        }
    };
}
static std::vector<p1_input_t> p1_input{};
static std::vector<instruction> p2_input{};

static inline bool read_input(std::istream& in) {
    std::string line{};
    p1_input_t p1{};

    static const std::regex re_before("Before:\\s*\\[(\\d+),\\s*(\\d+),\\s*(\\d+),\\s*(\\d+)\\s*\\]");
    static const std::regex re_after("After:\\s*\\[(\\d+),\\s*(\\d+),\\s*(\\d+),\\s*(\\d+)\\s*\\]");
    static const std::regex re_instr("(\\d+)\\s+(\\d+)\\s+(\\d+)\\s+(\\d+)");

    while (true) {
        std::string s_before{};
        std::string s_instr{};
        std::string s_after{};
        if (!std::getline(in, s_before)) return true;
        if (s_before.empty()) break;
        if (!std::getline(in, s_instr) || !std::getline(in, s_after)) {
            perr("Failed to read input: EOF encountered where data expected");
            return false;
        }

        std::smatch m_before{};
        std::smatch m_instr{};
        std::smatch m_after{};
        if (!std::regex_match(s_before, m_before, re_before) || !std::regex_match(s_instr, m_instr, re_instr) || !std::regex_match(s_after, m_after, re_after)) {
            if (std::regex_match(s_before, m_before, re_instr)) {
                pdbg("Reached end of input for part 1 on line '{}'", s_before);
                line = std::move(s_before);
                break;
            } else {
                perr("Failed to parse part 1 input:\n  '{}'\n  '{}'\n  '{}'", s_before, s_instr, s_after);
                return false;
            }
        }
        p1.before = {std::stoi(m_before[1].str()), std::stoi(m_before[2].str()), std::stoi(m_before[3].str()), std::stoi(m_before[4].str())};
        p1.instr  = {std::stoi(m_instr[1].str()),  std::stoi(m_instr[2].str()),  std::stoi(m_instr[3].str()),  std::stoi(m_instr[4].str())};
        p1.after  = {std::stoi(m_after[1].str()),  std::stoi(m_after[2].str()),  std::stoi(m_after[3].str()),  std::stoi(m_after[4].str())};
        p1_input.push_back(p1);

        if (!std::getline(in, line)) return true;
        if (!line.empty()) {
            perr("Unexpected input line '{}'", line);
            return false;
        }
    }

    while (line.empty()) {
        if (!std::getline(in, line)) return true;
    }
    while (true) {
        std::smatch m_instr{};
        if (!std::regex_match(line, m_instr, re_instr)) {
            perr("Failed to parse instruction '{}'", line);
            return false;
        }
        p2_input.push_back({std::stoi(m_instr[1].str()), std::stoi(m_instr[2].str()), std::stoi(m_instr[3].str()), std::stoi(m_instr[4].str())});
        if (!std::getline(in, line)) return true;
    }

    return true;
}

static inline void get_possible_instructions(const p1_input_t& p1, std::set<int>& matches) {
    for (const auto& desc : instructions) {
        regpack regs(p1.before);
        desc.proc(p1.instr, regs);
        if (p1.after == regs) {
            size_t idx = &desc - instructions.data();
            matches.insert(idx);
        }
    }
}

static inline bool resolve_instructions() {
    std::array<std::set<int>, instructions.size()> matches{};
    size_t resolved{0};

    for (const auto& p1 : p1_input) {
        get_possible_instructions(p1, matches[p1.instr.code]);
    }
    while (resolved < instructions.size()) {
        auto it = std::find_if(matches.begin(), matches.end(), [](const auto& s) {
            return s.size() == 1;
        });
        if (it == matches.end()) break;

        size_t idx = std::distance(matches.begin(), it);
        int our_idx = *it->begin();
        resolved_instructions[idx] = &instructions[our_idx];
        resolved++;

        for (auto& s : matches)
            s.erase(our_idx);
    }

    size_t final_count = std::count_if(resolved_instructions.begin(), resolved_instructions.end(), [](const instruction_desc* ip) {
        return ip != nullptr;
    });
    return final_count == instructions.size();
}

static inline void part1() {
    int ret{0};

    for (const auto& p1 : p1_input) {
        int count{0};
        for (const auto& desc : instructions) {
            regpack regs(p1.before);
            desc.proc(p1.instr, regs);
            if (p1.after == regs) count++;
        }
        if (count >= 3) ret++;
    }

    pout("PART 1: {}", ret);
}

static inline void part2() {
    regpack regs{};

    for (const auto& instr : p2_input) {
        const auto* pdesc = resolved_instructions[instr.code];
        pdesc->proc(instr, regs);
    }

    pout("PART 2: {}", regs[0]);
}

int main(int argc, char* argv[]) {
    if (argc >= 2) {
        std::ifstream ifs(argv[1]);
        if (!ifs.is_open()) {
            perr("Failed to open file '{}'", argv[1]);
            return 1;
        }
        if (!read_input(ifs)) return 1;
    } else {
        if (!read_input(std::cin)) return 1;
    }

    part1();
    if (!resolve_instructions()) {
        perr("Failed to resolve instructions!");
        return 1;
    }
    for (size_t i = 0; i < resolved_instructions.size(); i++) {
        pdbg("{:>2} -> {}", i, resolved_instructions[i] ? resolved_instructions[i]->name : "");
    }
    part2();

    return 0;
}
