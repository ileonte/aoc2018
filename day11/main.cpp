#include <vector>
#include <regex>
#include <optional>
#include <string>
#include <iostream>
#include <fmt/format.h>

#define pout(format, ...) fmt::print(::stdout, format "\n", ##__VA_ARGS__)

class summed_area {
public:
    using data_type = std::int64_t;

    summed_area(int size)
        : size_(std::abs(size))
        , data_(stride() * stride(), 0)
    {
        assert(size_ > 0);
    }

    const data_type& at(int x, int y) const {
        assert(std::clamp(x, 0, stride() - 1) == x);
        assert(std::clamp(y, 0, stride() - 1) == y);
        return data_[y * stride() + x];
    }

    void set(int x, int y, data_type v) {
        assert(std::clamp(x, 1, size_) == x);
        assert(std::clamp(y, 1, size_) == y);
        data_[y * stride() + x] = v + at(x, y - 1) + at(x - 1, y) - at(x - 1, y - 1);
    }

    data_type get(int x, int y, int w, int h) const {
        assert(std::clamp(x, 1, size_) == x);
        assert(std::clamp(y, 1, size_) == y);

        int max_x = x + w - 1;
        int max_y = y + h - 1;
        assert(std::clamp(max_x, 1, size_) == max_x);
        assert(std::clamp(max_y, 1, size_) == max_y);

        auto v1 = at(max_x, max_y);
        auto v2 = at(x - 1, y - 1);
        auto v3 = at(max_x, y - 1);
        auto v4 = at(x - 1, max_y);

        return v1 + v2 - v3 - v4;
    }

    inline int stride() const {
        return size_ + 1;
    }

private:
    int size_;
    std::vector<data_type> data_;
};

static constexpr const int max_area_size = 300;

static inline int power_level(int x, int y, int grid_serial) {
    assert(std::clamp(x, 1, max_area_size) == x);
    assert(std::clamp(y, 1, max_area_size) == y);

    int rack_id = x + 10;
    int ret = rack_id * y;
    ret += grid_serial;
    ret *= rack_id;
    ret = ((ret % 1000) / 100);
    ret -= 5;

    return ret;
}

static summed_area area(max_area_size);

struct selected_zone {
    int x{0};
    int y{0};
    summed_area::data_type v{0};
};
static inline selected_zone get_max_zone(int size) {
    assert(std::clamp(size, 1, max_area_size) == size);

    selected_zone ret{0, 0, std::numeric_limits<typeof(selected_zone::v)>::min()};
    for (int y = 1; y <= max_area_size - size + 1; y++) {
        for (int x = 1; x <= max_area_size - size + 1; x++) {
            auto tmp = area.get(x, y, size, size);
            if (tmp > ret.v) {
                ret.x = x;
                ret.y = y;
                ret.v = tmp;
            }
        }
    }
    return ret;
}

static inline void part1() {
    auto r = get_max_zone(3);
    pout("PART 1: {},{} ({})", r.x, r.y, r.v);
}

static inline void part2() {
    auto ret = get_max_zone(1);
    int max_size{1};
    for (int size = 2; size <= max_area_size; size++) {
        auto tmp = get_max_zone(size);
        if (tmp.v > ret.v) {
            max_size = size;
            ret = tmp;
        }
    }
    pout("PART 2: {},{},{}", ret.x, ret.y, max_size);
}

int main() {
    std::string line{};
    while (std::getline(std::cin, line)) {
        int grid_serial{std::stoi(line)};
        summed_area::data_type sum{};
        for (int y = 1; y <= max_area_size; y++) {
            for (int x = 1; x <= max_area_size; x++) {
                int pl = power_level(x, y, grid_serial);
                area.set(x, y, pl);
                sum += pl;
            }
        }

        part1();
        part2();
    }

    return 0;
}
