#include <vector>
#include <list>
#include <regex>
#include <optional>
#include <string>
#include <iostream>
#include <fstream>
#include <fmt/format.h>
#include <fmt/ostream.h>

#define pout(format, ...) fmt::print(::stdout, format "\n", ##__VA_ARGS__)
#define perr(format, ...) fmt::print(::stderr, format "\n", ##__VA_ARGS__)

struct board_t {
    std::vector<char> data{};
    size_t idx1{0};
    size_t idx2{1};
    size_t initial{};

    inline void init(size_t n) {
        std::string s = fmt::format("{}", n);
        assert(s.size() >= 2);
        data.clear();
        for (char c : s) {
            assert(std::clamp(c, '0', '9') == c);
            data.push_back(c);
        }
        idx1 = 0;
        idx2 = 1;
        initial = n;
    }

    inline size_t calculate_jump(size_t idx) const {
        size_t delta = (data[idx] - '0')  + 1;
        return (idx + delta) % data.size();
    }

    inline void play(size_t target, size_t len) {
        size_t mod = 1;
        size_t rolling = target;
        std::optional<std::string> part1{};
        std::optional<size_t> part2{};
        size_t delta{0};

        while (rolling) {
            mod *= 10;
            rolling /= 10;
            delta++;
        }
        rolling = initial;

        while (!part1.has_value() || !part2.has_value()) {
            int n = (data[idx1] - '0') + (data[idx2] - '0');
            int p1 = n / 10;
            int p2 = n - p1 * 10;

            if (p1) {
                data.push_back('0' + p1);
                if (!part2.has_value()) {
                    rolling = (rolling * 10 + p1) % mod;
                    if (rolling == target) part2 = data.size() - delta;
                }
            }

            data.push_back('0' + p2);
            if (!part2.has_value()) {
                rolling = (rolling * 10 + p2) % mod;
                if (rolling == target) part2 = data.size() - delta;
            }

            if (!part1.has_value() && data.size() >= (target + len))
                part1 = std::string_view(&data[target], len);

            idx1 = calculate_jump(idx1);
            idx2 = calculate_jump(idx2);
        }

        pout("PART 1: {}", part1.value());
        pout("PART 2: {}", part2.value());
    }
};
namespace fmt {
    template <>
    struct formatter<board_t> {
        template <typename ParseContext>
        constexpr auto parse(ParseContext &ctx) { return ctx.begin(); }

        template <typename FormatContext>
        auto format(const board_t& v, FormatContext &ctx) {
            auto ret = format_to(ctx.begin(), "");
            for (size_t i = 0; i < v.data.size(); i++) {
                if (i == v.idx1 && i == v.idx2) ret = format_to(ret, "{{{}}}", v.data[i]);
                else if (i == v.idx1) ret = format_to(ret, "({})", v.data[i]);
                else if (i == v.idx2) ret = format_to(ret, "[{}]", v.data[i]);
                else ret = format_to(ret, " {} ", v.data[i]);
            }
            return ret;
        }
    };
}

static board_t board{};
static std::vector<size_t> counts{};

static inline bool read_input(std::istream& in) {
    std::string line{};
    while (std::getline(in, line)) {
        counts.push_back(std::stoi(line));
    }
    return true;
}

int main(int argc, char* argv[]) {
    if (argc >= 2) {
        std::ifstream ifs(argv[1]);
        if (!ifs.is_open()) {
            perr("Failed to open file '{}'", argv[1]);
            return 1;
        }
        if (!read_input(ifs)) return 1;
    } else {
        if (!read_input(std::cin)) return 1;
    }

    for (auto count : counts) {
        board.init(37);
        board.play(count, 10);
    }

    return 0;
}
