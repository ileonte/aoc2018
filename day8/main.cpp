#include <iostream>
#include <fmt/format.h>

#define pout(format, ...) fmt::print(::stdout, format "\n", ##__VA_ARGS__)
#define perr(format, ...) fmt::print(::stderr, format "\n", ##__VA_ARGS__)
#if defined(NDEBUG)
#define pdbg(format, ...)
#else
#define pdbg(format, ...) pout(format, ##__VA_ARGS__)
#endif

struct counters {
    int meta_sum{0};
    int value{0};
};

static inline counters read_node(int id = 0) {
    int child_count{0};
    int meta_count{0};
    counters ret{};
    std::array<counters, 128> children{};

    std::cin >> child_count;
    std::cin >> meta_count;

    for (int i = 0; i < child_count; i++) {
        children[i] = read_node(id + 1);
        ret.meta_sum += children[i].meta_sum;
    }

    int meta{0};
    if (!child_count) {
        for (int i = 0; i < meta_count; i++) {
            std::cin >> meta;
            ret.meta_sum += meta;
            ret.value += meta;
        }
    } else {
        for (int i = 0; i < meta_count; i++) {
            std::cin >> meta;
            ret.meta_sum += meta;
            if (meta < 1 || meta > child_count) continue;
            ret.value += children[meta - 1].value;
        }
    }

    return ret;
}

int main() {
    auto r = read_node();
    pout("PART 1: {}", r.meta_sum);
    pout("PART 2: {}", r.value);
    return 0;
}
