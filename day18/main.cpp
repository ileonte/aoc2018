#include <set>
#include <vector>
#include <array>
#include <list>
#include <regex>
#include <optional>
#include <string>
#include <iostream>
#include <fstream>
#include <fmt/format.h>
#include <fmt/ostream.h>

#define pout(format, ...) fmt::print(::stdout, format "\n", ##__VA_ARGS__)
#define perr(format, ...) fmt::print(::stderr, format "\n", ##__VA_ARGS__)
#if defined(NDEBUG)
#define pdbg(format, ...)
#else
#define pdbg(format, ...) pout(format, ##__VA_ARGS__)
#endif

namespace fmt {
    template <typename T>
    struct formatter<std::vector<T>> {
        template <typename ParseContext>
        constexpr auto parse(ParseContext &ctx) { return ctx.begin(); }

        template <typename FormatContext>
        auto format(const std::vector<T> &v, FormatContext &ctx) {
            auto ret = format_to(ctx.begin(), "[");
            for (size_t i = 0; i < v.size(); i++)
                ret = format_to(ret, "{}{}", v[i], i < v.size() - 1 ? ", " : "");
            return format_to(ret, "]");
        }
    };

    template <typename T, size_t N>
    struct formatter<std::array<T, N>> {
        template <typename ParseContext>
        constexpr auto parse(ParseContext& ctx) { return ctx.begin(); }

        template <typename FormatContext>
        auto format(const std::array<T, N>& arr, FormatContext& ctx) {
            auto ret = format_to(ctx.begin(), "[");
            for (size_t i = 0; i < arr.size(); i++)
                ret = format_to(ret, "{}{}", arr[i], i < arr.size() - 1 ? ", " : "");
            return format_to(ret, "]");
        }
    };
}

struct point {
    int x{0};
    int y{0};
};

class board {
public:
    struct neighbour_counts {
        int open_ground{0};
        int trees{0};
        int lumberyard{0};
    };

    inline board() = default;
    inline board(const std::vector<std::string>& init_data) {
        if (init_data.empty()) return;

        int w = init_data[0].size();
        int h = init_data.size();
        std::vector<char> data(w * h, '.');

        for (size_t y = 0; y < init_data.size(); y++) {
            const auto& line = init_data[y];
            if ((int)line.size() != w) {
                perr("Error: line {} has length {}, expecting {}", y + 1, line.size(), w);
                return;
            }
            for (size_t x = 0; x < line.size(); x++) {
                switch (line[x]) {
                    case '.': [[fallthrough]];
                    case '#': [[fallthrough]];
                    case '|': {
                        data[y * w + x] = line[x];
                        break;
                    }
                    default: {
                        perr("Error: unexpected character '{}' in input at {}:{}", line[x], y + 1, x + 1);
                        return;
                    }
                }
            }
        }

        front_ = std::move(data);
        back_  = std::vector<char>(front_.size(), '.');
        width_ = w;
        height_ = h;
    }

    inline void print() const {
        for (int y = 0; y < height_; y++)
            pout("{}", std::string_view(front_.data() + y * width_, width_));
    }

    inline bool contains(int x, int y) const {
        return std::clamp(x, 0, width_ - 1) == x && std::clamp(y, 0, height_ - 1) == y;
    }

    neighbour_counts count_neighbours(int x, int y) const {
        assert(contains(x, y));

        static constexpr const auto do_count = [](const board* b, int x, int y, neighbour_counts& counts) {
            if (!b->contains(x, y)) return ;
            switch (b->at(x, y)) {
                case '.': { counts.open_ground++; break; }
                case '#': { counts.lumberyard++; break; }
                case '|': { counts.trees++; break; }
            }
        };

        neighbour_counts ret{};
        do_count(this, x - 1, y - 1, ret);
        do_count(this, x    , y - 1, ret);
        do_count(this, x + 1, y - 1, ret);
        do_count(this, x - 1, y    , ret);
        do_count(this, x + 1, y    , ret);
        do_count(this, x - 1, y + 1, ret);
        do_count(this, x    , y + 1, ret);
        do_count(this, x + 1, y + 1, ret);
        return  ret;
    }

    inline board(const board&) = delete;
    inline board& operator=(const board&) = delete;
    inline board(board&&) = default;
    inline board& operator=(board&&) = default;

    inline void clear() {
        front_.clear();
        back_.clear();
        width_ = 0;
        height_ = 0;
    }

    inline bool is_valid() const { return !front_.empty(); }
    inline int width() const { return width_; }
    inline int height() const { return height_; }

    const char& at(int x, int y) const {
        assert(contains(x, y));
        return front_.at(y * width_ + x);
    }

    void play() {
        for (int y = 0; y < height_; y++) {
            for (int x = 0; x < width_; x++) {
                auto [open, trees, lumberyards] = count_neighbours(x, y);
                switch (at(x, y)) {
                    case '.': {
                        at_back(x, y) = (trees >= 3) ? '|' : '.';
                        break;
                    }
                    case '|': {
                        at_back(x, y) = (lumberyards >= 3) ? '#' : '|';
                        break;
                    }
                    case '#': {
                        at_back(x, y) = (lumberyards >= 1 && trees >= 1) ? '#' : '.';
                        break;
                    }
                }
            }
        }
        swap_buffers();
    }

    const auto& data() const { return front_; };

private:
    void swap_buffers() { std::swap(front_, back_); };
    char &at_back(int x, int y) {
        assert(contains(x, y));
        return back_.at(y * width_ + x);
    }

    std::vector<char> front_{};
    std::vector<char> back_{};
    int width_{0};
    int height_{0};
};

static board the_board{};

static inline bool read_input(std::istream& in) {
    std::string line{};
    std::vector<std::string> lines{};

    while (std::getline(in, line)) {
        lines.push_back(line);
    }

    the_board = board(lines);
    if (!the_board.is_valid()) return false;

    return true;
}

static inline void part1() {
    for (int i = 0; i < 10; i++)
        the_board.play();
    auto trees = std::count_if(the_board.data().begin(), the_board.data().end(), [](char c) { return c == '|'; });
    auto lumberyards = std::count_if(the_board.data().begin(), the_board.data().end(), [](char c) { return c == '#'; });
    pout("PART 1: {}", trees * lumberyards);
}

/*
 * bullshit implementation based on observation
 */
static inline void part2() {
    std::array<std::pair<int, int>, 7> vals {{
        {587, 336},
        {614, 342},
        {599, 338},
        {596, 349},
        {595, 351},
        {583, 347},
        {602, 336},
    }};
    const auto& p = vals[((1000000000 - 500) / 100) % vals.size()];
    pout("PART 2: {}", p.first * p.second);
}

int main(int argc, char* argv[]) {
    if (argc >= 2) {
        std::ifstream ifs(argv[1]);
        if (!ifs.is_open()) {
            perr("Failed to open file '{}'", argv[1]);
            return 1;
        }
        if (!read_input(ifs)) return 1;
    } else {
        if (!read_input(std::cin)) return 1;
    }

    part1();
    part2();

    return 0;
}
