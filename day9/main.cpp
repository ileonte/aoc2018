#include <regex>
#include <string>
#include <list>
#include <iostream>
#include <fmt/format.h>

#define pout(format, ...) fmt::print(::stdout, format "\n", ##__VA_ARGS__)
#define perr(format, ...) fmt::print(::stderr, format "\n", ##__VA_ARGS__)

namespace fmt {
    template <typename T>
    struct formatter<std::list<T>> {
        template <typename ParseContext>
        constexpr auto parse(ParseContext& ctx) { return ctx.begin(); }

        template <typename FormatContext>
        auto format(const std::list<T>& l, FormatContext& ctx) {
            auto ret = format_to(ctx.begin(), "[");
            for (auto it = l.begin(); it != l.end(); ++it)
                ret = format_to(ret, "{:>4}{}", *it, std::next(it) == l.end() ? "" : ", ");
            return format_to(ret, "]");
        }
    };
}

template <typename Container, typename Iterator>
static inline auto next(Container& container, Iterator& it, size_t count) {
    auto ret = it;
    for (size_t i = 0; i < count; i++) {
        ++ret;
        if (ret == container.end()) ret = container.begin();
    }
    return ret == container.begin() ? container.end() : ret;
}

template <typename Container, typename Iterator>
static inline auto prev(Container& container, Iterator& it, size_t count) {
    auto ret = it;
    for (size_t i = 0; i < count; i++) {
        --ret;
        if (ret == container.begin()) ret = container.end();
    }
    return ret == container.end() ? container.begin() : ret;
}

static inline size_t play_the_game(size_t player_count, size_t highest_marble) {
    std::list<size_t > board{};
    size_t current_marble{0};
    size_t current_player{0};
    std::vector<size_t > player_scores(player_count, 0);

    board.push_back(0);

    auto current_position = board.begin();
    while (true) {
        current_marble += 1;
        if (current_marble > highest_marble) break;

        current_player += 1;
        if (current_player > player_count) current_player = 1;

        if (current_marble % 23 == 0) {
            auto p = prev(board, current_position, 7);
            player_scores[current_player - 1] += (current_marble + *p);
            current_position = next(board, p, 1);
            board.erase(p);
        } else {
            auto n = next(board, current_position, 2);
            current_position = board.insert(n, current_marble);
        }
    }

    return *std::max_element(player_scores.begin(), player_scores.end());
}

int main() {
    std::string input{};
    static const std::regex re_in("(\\d+) players; last marble is worth (\\d+) points");

    while (std::getline(std::cin, input)) {
        if (input.front() == '#') continue;

        std::smatch m{};
        if (!std::regex_match(input, m, re_in)) {
            perr("Failed to parse line '{}'", input);
            return 1;
        }

        size_t player_count = std::stoi(m[1]);
        size_t highest_marble = std::stoi(m[2]);

        pout("PART 1: {}", play_the_game(player_count, highest_marble));
        pout("PART 2: {}", play_the_game(player_count, highest_marble * 100));
    }

    return 0;
}
