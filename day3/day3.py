import sys
import re

claims = []
board = []
board_size = 1000
for i in range(board_size):
    board.append([0] * 1000)

claim_re = re.compile(
    '^\\s*#(?P<id>\\d+)\\s*@\\s*(?P<x>\\d+)\\s*,\\s*(?P<y>\\d+)\\s*:\\s*(?P<w>\\d+)\\s*x\\s*(?P<h>\\d+)\\s*$'
)
def parse_claim(s):
    global claim_re
    global claims
    m = re.fullmatch(claim_re, s)
    if m:
        claims.append([
            int(m.group('id')),
            int(m.group('x')),
            int(m.group('y')),
            int(m.group('w')),
            int(m.group('h')),
        ])
    else:
        print("FAILED TO PARSE: '{}'".format(s))

def part1():
    global claims
    global board

    total = 0
    for claim in claims:
        for y in range(claim[4]):
            yy = claim[2] + y

            for x in range(claim[3]):
                xx = claim[1] + x

                if board[yy][xx] == 1:
                    total += 1
                board[yy][xx] += 1

    return total

def part2():
    global claims
    global board

    good_claim = None

    for claim in claims:
        local_overlaps = 0

        for y in range(claim[4]):
            yy = claim[2] + y

            for x in range(claim[3]):
                xx = claim[1] + x

                if board[yy][xx] > 1:
                    local_overlaps += 1

        if local_overlaps == 0:
            return claim[0]

    return None

while True:
    try:
        s = input()
        parse_claim(s)
    except EOFError:
        break
    else:
        pass

print("part 1: {}".format(part1()))
print("part 2: {}".format(part2()))
