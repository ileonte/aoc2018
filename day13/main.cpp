#include <vector>
#include <regex>
#include <optional>
#include <string>
#include <iostream>
#include <fstream>
#include <fmt/format.h>

#define pout(format, ...) fmt::print(::stdout, format "\n", ##__VA_ARGS__)
#define perr(format, ...) fmt::print(::stderr, format "\n", ##__VA_ARGS__)
#if defined(NDEBUG)
#define pdbg(format, ...)
#else
#define pdbg(format, ...) pout(format, ##__VA_ARGS__)
#endif

namespace fmt {
    template <typename T>
    struct formatter<std::vector<T>> {
        template <typename ParseContext>
        constexpr auto parse(ParseContext &ctx) { return ctx.begin(); }

        template <typename FormatContext>
        auto format(const std::vector<T> &v, FormatContext &ctx) {
            auto ret = format_to(ctx.begin(), "[");
            for (size_t i = 0; i < v.size(); i++)
                ret = format_to(ret, "{}{}", v[i], i < v.size() - 1 ? ", " : "");
            return format_to(ret, "]");
        }
    };
}

enum class direction_change : std::uint8_t {
    left,
    straight,
    right,
};
inline direction_change operator++(direction_change& self, int) {
    direction_change ret{self};
    switch (self) {
        case direction_change::left: {
            self = direction_change::straight;
            break;
        }
        case direction_change::straight: {
            self = direction_change::right;
            break;
        }
        case direction_change::right: {
            self = direction_change::left;
            break;
        }
    }
    return ret;
}
inline direction_change operator+(direction_change d, char c) {
    if (c == '+') d++;
    return d;
}

enum class direction : std::uint8_t {
    up, down, left, right,
};
inline direction operator+(direction d, direction_change c) {
    switch (c) {
        case direction_change::straight: return d;
        case direction_change::left: {
            switch (d) {
                case direction::up: return direction::left;
                case direction::down: return direction::right;
                case direction::left: return direction::down;
                case direction::right: return direction::up;
            }
        }
        case direction_change::right: {
            switch (d) {
                case direction::up: return direction::right;
                case direction::down: return direction::left;
                case direction::left: return direction::up;
                case direction::right: return direction::down;
            }
        }
    }
    return d;
}
inline direction operator+(direction d, char c) {
    switch (c) {
        case '\\': {
            switch (d) {
                case direction::up: return direction::left;
                case direction::down: return direction::right;
                case direction::left: return direction::up;
                case direction::right: return direction::down;
            }
        }
        case '/': {
            switch (d) {
                case direction::up: return direction::right;
                case direction::down: return direction::left;
                case direction::left: return direction::down;
                case direction::right: return direction::up;
            }
        }
    }
    return d;
}
inline char to_char(direction d) {
    switch (d) {
        case direction::up: return '^';
        case direction::down: return 'v';
        case direction::left: return '<';
        case direction::right: return '>';
    }
    return 'Z';
}

struct position {
    int x{0};
    int y{0};

    inline bool operator==(const position& other) const {
        return x == other.x && y == other.y;
    }
};
inline position operator+(const position& p, direction d) {
    switch (d) {
        case direction::up: return {p.x, p.y - 1};
        case direction::down: return {p.x, p.y + 1};
        case direction::left: return {p.x - 1, p.y};
        case direction::right: return {p.x + 1, p.y};
    }
    return p;
}

struct cart {
    position pos{};
    direction dir{};
    direction_change dir_chg{direction_change::left};
    int id{-1};
    bool crashed{false};

    using maybe_cart = std::optional<cart>;
    static inline maybe_cart from_input_data(int x, int y, char c) {
        position pos{x, y};
        direction_change ch{direction_change::left};
        switch (c) {
            case '^': return cart{pos, direction::up, ch};
            case 'v': return cart{pos, direction::down, ch};
            case '<': return cart{pos, direction::left, ch};
            case '>': return cart{pos, direction::right, ch};
            default: return {};
        }
    }
};
inline bool operator<(const cart& c1, const cart& c2) {
    if (c1.crashed != c2.crashed) {
        if (!c1.crashed) return true;
        return false;
    }
    if (c1.pos.y < c2.pos.y) return true;
    if (c1.pos.y == c2.pos.y) return (c1.pos.x < c2.pos.x);
    return false;
}

struct board {
    std::vector<std::vector<char>> data{};
    int width{0};
    int height{0};

    board(const board&) = delete;
    board& operator=(const board&) = delete;

    board(board&&) = default;
    board& operator=(board&&) = default;

    board() = default;

    board(int w, int h) {
        assert(w > 0);
        assert(h > 0);
        width = w;
        height = h;
        data = std::vector<std::vector<char>>(height + 2, std::vector<char>(width + 2, ' '));
    }

    inline char& raw_at(int x, int y) {
        assert(is_valid());
        assert(std::clamp(x, 0, (int)data[0].size()) == x);
        assert(std::clamp(y, 0, (int)data.size()) == y);
        return data[y][x];
    }
    inline char& at(int x, int y) {
        assert(is_valid());
        assert(std::clamp(x, 0, width) == x);
        assert(std::clamp(y, 0, height) == y);
        return data[y + 1][x + 1];
    }

    inline const char& raw_at(int x, int y) const {
        assert(is_valid());
        assert(std::clamp(x, 0, (int)data[0].size()) == x);
        assert(std::clamp(y, 0, (int)data.size()) == y);
        return data[y][x];
    }
    inline const char& at(int x, int y) const {
        assert(is_valid());
        assert(std::clamp(x, 0, width) == x);
        assert(std::clamp(y, 0, height) == y);
        return data[y + 1][x + 1];
    }

    inline char& at(const position& p) {
        assert(is_valid());
        assert(std::clamp(p.x, 0, width) == p.x);
        assert(std::clamp(p.y, 0, height) == p.y);
        return data[p.y + 1][p.x + 1];
    }
    inline char& raw_at(const position& p) {
        assert(is_valid());
        assert(std::clamp(p.x, 0, (int)data[0].size()) == p.x);
        assert(std::clamp(p.y, 0, (int)data.size()) == p.y);
        return data[p.y][p.x];
    }

    inline const char& at(const position& p) const {
        assert(is_valid());
        assert(std::clamp(p.x, 0, width) == p.x);
        assert(std::clamp(p.y, 0, height) == p.y);
        return data[p.y + 1][p.x + 1];
    }
    inline const char& raw_at(const position& p) const {
        assert(is_valid());
        assert(std::clamp(p.x, 0, (int)data[0].size()) == p.x);
        assert(std::clamp(p.y, 0, (int)data.size()) == p.y);
        return data[p.y][p.x];
    }

    inline bool contains(const position& p) const {
        if (!is_valid()) return false;
        return (std::clamp(p.x, 0, width) == p.x) && (std::clamp(p.y, 0, height) == p.y);
    }

    bool add_line(const std::string& line, int y, std::vector<cart>& carts) {
        static int cart_id{0};
        assert(line.size() <= (size_t)width);

        int x{0};
        for (auto c : line) {
            switch (c) {
                case '^': [[fallthrough]];
                case 'v': {
                    auto cart = cart::from_input_data(x, y, c);
                    assert(cart.has_value());
                    at(x, y) = '|';
                    carts.push_back(cart.value());
                    carts.back().id = cart_id++;
                    break;
                }
                case '<': [[fallthrough]];
                case '>': {
                    auto cart = cart::from_input_data(x, y, c);
                    assert(cart.has_value());
                    at(x, y) = '-';
                    carts.push_back(cart.value());
                    carts.back().id = cart_id++;
                    break;
                }
                case ' ': [[fallthrough]];
                case '|': [[fallthrough]];
                case '-': [[fallthrough]];
                case '/': [[fallthrough]];
                case '\\': [[fallthrough]];
                case '+': {
                    at(x, y) = c;
                    break;
                }
                default: return false;
            }
            x++;
        }
        return true;
    }

    inline bool is_valid() const { return !data.empty(); }

    inline void move_cart(cart& cart) const {
        position new_pos = cart.pos + cart.dir;

        assert(contains(new_pos));
        assert(at(new_pos) != ' ');

        cart.pos = new_pos;

        switch (at(new_pos)) {
            case '\\': [[fallthrough]];
            case '/': {
                cart.dir = cart.dir + at(new_pos);
                break;
            }
            case '+': {
                cart.dir = cart.dir + cart.dir_chg;
                break;
            }
        }
        cart.dir_chg = cart.dir_chg + at(new_pos);
    }
};

namespace fmt {
    template <>
    struct formatter<direction_change> {
        template <typename ParseContext>
        constexpr auto parse(ParseContext &ctx) { return ctx.begin(); }
        template <typename FormatContext>
        auto format(const direction_change& v, FormatContext &ctx) {
            auto ret = format_to(ctx.begin(), "");
            switch (v) {
                case direction_change::left: {
                    ret = format_to(ret, "<<");
                    break;
                }
                case direction_change::straight: {
                    ret = format_to(ret, "<>");
                    break;
                }
                case direction_change::right: {
                    ret = format_to(ret, ">>");
                    break;
                }
            }
            return ret;
        }
    };
    template <>
    struct formatter<direction> {
        template <typename ParseContext>
        constexpr auto parse(ParseContext &ctx) { return ctx.begin(); }
        template <typename FormatContext>
        auto format(const direction& v, FormatContext &ctx) {
            return format_to(ctx.begin(), "({})", to_char(v));
        }
    };
    template <>
    struct formatter<position> {
        template <typename ParseContext>
        constexpr auto parse(ParseContext &ctx) { return ctx.begin(); }
        template <typename FormatContext>
        auto format(const position& v, FormatContext &ctx) {
            return format_to(ctx.begin(), "{{{:>3},{:>3}}}", v.x, v.y);
        }
    };
    template <>
    struct formatter<cart> {
        template <typename ParseContext>
        constexpr auto parse(ParseContext &ctx) { return ctx.begin(); }
        template <typename FormatContext>
        auto format(const cart& v, FormatContext &ctx) {
            return format_to(ctx.begin(), "{{{}: {} {} {}}}", v.id, v.pos, v.dir, v.dir_chg);
        }
    };
}

static std::vector<cart> carts{};
static board the_board{};

static inline bool read_input(std::istream& in) {
    std::vector<std::string> lines{};
    int max_len{0};

    std::string line{};
    while (std::getline(in, line)) {
        max_len = std::max((size_t)max_len, line.size());
        lines.push_back(std::move(line));
    }

    board brd(max_len, lines.size());
    for (const auto& line : lines) {
        if (!brd.add_line(line, &line - lines.data(), carts)) {
            perr("Failed to parse line '{}'", line);
            return false;
        }
    }

    the_board = std::move(brd);

    return true;
}

static inline int tick(position* first_collision = nullptr) {
    int crashes{0};

    std::sort(carts.begin(), carts.end());
    for (auto& current_cart : carts) {
        if (current_cart.crashed) continue;

        the_board.move_cart(current_cart);
        auto it = std::find_if(carts.begin(), carts.end(), [&](cart& c) -> bool {
            return (&c != &current_cart) && (c.pos == current_cart.pos);
        });
        if (it != carts.end()) {
            current_cart.crashed = true;
            it->crashed = true;
            crashes++;
        }
    }

    if (crashes) {
        if (first_collision) {
            auto it = std::find_if(carts.begin(), carts.end(), [](cart& c) -> bool { return c.crashed; });
            assert(it != carts.end());
            *first_collision = it->pos;
        }

        std::sort(carts.begin(), carts.end());
        auto it = std::find_if(carts.begin(), carts.end(), [](cart& c) -> bool { return c.crashed; });
        assert(it != carts.end());
        carts.erase(it, carts.end());
    }

    return crashes;
}

static inline void part1() {
    position first_collision{};

    while (true) {
        if (tick(&first_collision)) break;
    }

    pout("PART 1: {},{}", first_collision.x, first_collision.y);
}

static inline void part2() {
    while (carts.size() > 1) {
        tick();
    }

    if (carts.size())
        pout("PART 2: {},{}", carts[0].pos.x, carts[0].pos.y);
    else
        pout("PART 2: everyone is DEAD!");
}

int main(int argc, char* argv[]) {
    if (argc >= 2) {
        std::ifstream ifs(argv[1]);
        if (!ifs.is_open()) {
            perr("Failed to open file '{}'", argv[1]);
            return 1;
        }
        if (!read_input(ifs)) return 1;
    } else {
        if (!read_input(std::cin)) return 1;
    }

    part1();
    part2();

    return 0;
}
