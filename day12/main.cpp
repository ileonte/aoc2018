#include <vector>
#include <list>
#include <regex>
#include <optional>
#include <string>
#include <iostream>
#include <fstream>
#include <fmt/format.h>
#include <fmt/ostream.h>

#define pout(format, ...) fmt::print(::stdout, format "\n", ##__VA_ARGS__)
#define perr(format, ...) fmt::print(::stderr, format "\n", ##__VA_ARGS__)
#if defined(NDEBUG)
#define pdbg(format, ...)
#else
#define pdbg(format, ...) pout(format, ##__VA_ARGS__)
#endif

namespace fmt {
    template <typename T>
    struct formatter<std::vector<T>> {
        template <typename ParseContext>
        constexpr auto parse(ParseContext &ctx) { return ctx.begin(); }

        template <typename FormatContext>
        auto format(const std::vector<T> &v, FormatContext &ctx) {
            auto ret = format_to(ctx.begin(), "[");
            for (size_t i = 0; i < v.size(); i++)
                ret = format_to(ret, "{}{}", v[i], i < v.size() - 1 ? ", " : "");
            return format_to(ret, "]");
        }
    };
    template <typename T>
    struct formatter<std::list<T>> {
        template <typename ParseContext>
        constexpr auto parse(ParseContext& ctx) { return ctx.begin(); }

        template <typename FormatContext>
        auto format(const std::list<T>& l, FormatContext& ctx) {
            auto ret = format_to(ctx.begin(), "[");
            for (auto it = l.begin(); it != l.end(); ++it)
                ret = format_to(ret, "{:>4}{}", *it, std::next(it) == l.end() ? "" : ", ");
            return format_to(ret, "]");
        }
    };
}

class pattern {
public:
    using maybe_pattern = std::optional<pattern>;

    pattern(const pattern&) = default;
    pattern& operator=(const pattern&) = default;

    static inline maybe_pattern from_text_rule(const std::string& line) {
        static const std::regex re_rule("([#.]{5}) => (#|.)");
        std::smatch m{};

        if (!std::regex_match(line, m, re_rule))
            return {};

        pattern ret{};
        auto s = m[1].str();
        std::memcpy(ret.match_rule_, s.c_str(), 5);
        ret.result_ = m[2].str().front();

        return ret;
    }

    inline const auto& rule() const { return match_rule_; }
    inline auto result() const { return result_; };

    inline bool matches(const char (&input)[5]) const {
        return std::memcmp(input, match_rule_, 5) == 0;
    }

    inline std::string to_string() const {
        return fmt::format("{} => {}", std::string_view(match_rule_, 5), result_);
    }

private:
    pattern() = default;

    char match_rule_[5]{'.', '.', '.', '.', '.'};
    char result_{'.'};
};

class state {
public:
    using maybe_state = std::optional<state>;

    struct state_entry {
        int id{0};
        char v{'.'};
    };
    using state_list = std::list<state_entry>;
    using state_it = state_list::iterator;
    using state_cit = state_list::const_iterator;

    state(const state&) = delete;
    state& operator=(const state&) = delete;
    state(state&&) = default;
    state& operator=(state&&) = default;

    static inline maybe_state from_text(const std::string& line) {
        static const std::regex re_state("initial state:\\s+([#.]+)");
        std::smatch m{};

        if (!std::regex_match(line, m, re_state))
            return {};

        state ret{};
        int id{0};

        ret.first_ = ret.last_ = ret.lst_.end();
        for (const auto c : m[1].str()) {
            auto it = ret.lst_.insert(ret.lst_.end(), {id++, c});
            if (it->v == '#') {
                if (ret.first_ == ret.lst_.end()) ret.first_ = it;
                ret.last_ = it;
            }
        }
        if (ret.first_ == ret.lst_.end()) ret.first_ = ret.lst_.begin();
        if (ret.last_ == ret.lst_.end()) ret.last_ = std::prev(ret.lst_.end());
        return ret;
    }

    inline void add_pattern(const pattern& p) { patterns_.push_back(p); }
    inline const auto& patterns() const { return patterns_; }

    inline auto first() const { return state_cit(first_); }
    inline auto last() const { return state_cit(last_); }
    inline const auto& list() const { return lst_; }

    inline void match_data_for(state_cit entry, char (&data)[5]) const {
        data[0] = data[1] = data[3] = data[4] = '.';
        data[2] = (entry == lst_.cend() ? '.' : entry->v);

        auto it = (entry == lst_.cend() ? entry : std::next(entry));
        for (int i = 3; i < 5; i++) {
            if (it == lst_.cend()) break;
            data[i] = it->v;
            it = std::next(it);
        }

        if (entry == lst_.cbegin()) return;
        it = std::prev(entry);
        for (int i = 1; i > -1; i--) {
            data[i] = it->v;
            if (it == lst_.cbegin()) break;
            it = std::prev(it);
        }
    }
    inline void match_data_for(int id, char (&data)[5]) {
        for (auto it = lst_.cbegin(); it != lst_.cend(); it++) {
            if (it->id != id) continue;
            match_data_for(it, data);
            return;
        }
        match_data_for(lst_.cend(), data);
    }

    inline std::string to_string() const {
        std::stringstream ss;
        fmt::print(ss, "{:>3}({:>3}) ", first_->id, lst_.front().id);
        for (auto it = lst_.begin(); it != lst_.end(); it++) {
            if (it == first_ || it == last_) ss << '(' << it->v << ')';
            else ss << it->v;
        }
        fmt::print(ss, " {:>3}({:>4})", last_->id, lst_.back().id);
        return ss.str();
    }

    inline int64_t evolve() {
        static thread_local std::vector<char> scratch_pad{};
        int64_t result{0};
        char data[5]{};

        if (first_->v != '#') return 0;
        while (lst_.front().id > first_->id - 2)
            lst_.insert(lst_.begin(), {lst_.front().id - 1, '.'});

        if (last_->v != '#') return 0;
        while (lst_.back().id < last_->id + 2)
            lst_.insert(lst_.end(), {lst_.back().id + 1, '.'});

        scratch_pad.reserve(std::max(scratch_pad.capacity(), 2 * lst_.size()));
        scratch_pad.clear();
        for (auto it = lst_.begin(); it != lst_.end(); it++)
            scratch_pad.push_back('.');

        auto rit = scratch_pad.begin();
        for (auto it = lst_.begin(); it != lst_.end(); it++) {
            match_data_for(it, data);
            auto pit = std::find_if(patterns_.begin(), patterns_.end(), [&](const pattern& p) -> bool {
                return p.matches(data);
            });
            if (pit != patterns_.end()) {
                char c = pit->result();
                if (c == '#') result += it->id;
                *rit = c;
            }
            rit++;
        }

        auto it = lst_.begin();
        for (auto c : scratch_pad) {
            it->v = c;
            it = std::next(it);
        }
        first_ = lst_.begin();
        for (auto it = lst_.begin(); it != lst_.end(); it++) {
            if (it->v == '#') {
                first_ = it;
                break;
            }
        }
        last_ = lst_.begin();
        for (auto it = lst_.rbegin(); it != lst_.rend(); it++) {
            if (it->v == '#') {
                last_ = std::prev(it.base());
                break;
            }
        }

        return result;
    }

private:
    state() = default;

    state_list lst_{};
    state_it first_{};
    state_it last_{};
    std::vector<pattern> patterns_{};
};

static inline state::maybe_state parse_input(std::istream& in) {
    std::string line{};
    std::getline(in, line);
    auto parsed_state = state::from_text(line);

    if (!parsed_state) {
        perr("Failed to parse state from line '{}'", line);
        return {};
    }

    std::getline(in, line);

    while (std::getline(in, line)) {
        auto ret = pattern::from_text_rule(line);
        if (!ret) {
            perr("Failed to parse pattern from line '{}'", line);
            return {};
        }
        parsed_state.value().add_pattern(ret.value());
    }

    return parsed_state;
}

int main(int argc, char* argv[]) {
    state::maybe_state parsed_state{};

    if (argc >= 2) {
        std::ifstream ifs(argv[1]);
        if (!ifs.is_open()) {
            perr("Failed to open file '{}'", argv[1]);
            return 1;
        }
        parsed_state = parse_input(ifs);
    } else {
        parsed_state = parse_input(std::cin);
    }
    if (!parsed_state) return 1;

    state st = std::move(parsed_state.value());
    pout("{:>3} : {}", 0, st.to_string());

    int64_t r{0};
    for (int i = 0; i < 20; i++){
        r = st.evolve();
        pout("{:>3} : {}", i + 1, st.to_string());
    }
    pout("PART 1: {}", r);


    for (int64_t i = 21; i < 50000000000; i++) {
        r = st.evolve();
        if (!(i % 10000)) pout("{} {} {}", i, r, st.list().size());
    }
    pout("PART 2: {}", r);

    return 0;
}

/*
10000 731776 10104
20000 1461776 20104
30000 2191776 30104
40000 2921776 40104
50000 3651776 50104
60000 4381776 60104
70000 5111776 70104
80000 5841776 80104
90000 6571776 90104
100000 7301776 100104
110000 8031776 110104
120000 8761776 120104
130000 9491776 130104
140000 10221776 140104
150000 10951776 150104
*/
