import sys

def m_ord(c):
    return ord(c) - ord('a')

def chrarray():
    return [0] * (m_ord('z') - m_ord('a') + 1)

def heatmap(s):
    counts = chrarray()
    for c in s:
        counts[m_ord(c)] += 1
    return counts

def part1(s, m):
    c_2 = 0
    c_3 = 0
    i_2 = chrarray()
    i_3 = chrarray()

    for n in range(len(m)):
        if m[n] == 2:
            i_2[n] = 1
        if m[n] == 3:
            i_2[n] = 0
            i_3[n] = 1
        if m[n] > 3:
            i_3[n] = 0
    c_2 = min(1, sum(i_2))
    c_3 = min(1, sum(i_3))
    return [c_2, c_3]

def part2(ss):
    for first_idx in range(len(ss) - 1):
        first_str = ss[first_idx]

        for cmp_idx in range(first_idx + 1, len(ss)):
            cmp_str = ss[cmp_idx]

            diff = 0
            for n in range(len(cmp_str)):
                if first_str[n] != cmp_str[n]:
                    diff += 1

            if diff == 1:
                m = []
                for n in range(len(cmp_str)):
                    if first_str[n] == cmp_str[n]:
                        m.append(first_str[n])
                s = ''.join(m)
                print("'{}' <-> '{}' -> '{}' ({})".format(first_str, cmp_str, s, m))

rets = [0, 0]
strs = []

while True:
    try:
        s = input()
        m = heatmap(s)

        strs.append(s)

        r = part1(s, m)
        rets[0] += r[0]
        rets[1] += r[1]
    except EOFError:
        break
    else:
        pass

print('part1: {}'.format(rets[0] * rets[1]))

part2(strs)
