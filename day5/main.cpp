#include <string>
#include <iostream>
#include <string_view>
#include <fmt/format.h>

#define pout(format, ...) fmt::print(::stdout, format "\n", ##__VA_ARGS__)

bool are_opposites(int c1, int c2) {
    return std::abs(c1 - c2) == ('a' - 'A');
}

size_t react(const std::string& input, std::string& react_buffer, char filter = 0) {
    react_buffer.clear();
    react_buffer.push_back(input.front());

    for (size_t i = 1; i < input.size(); i++) {
        char c = input[i];

        if (filter == c || are_opposites(filter, c))
            continue;

        if (are_opposites(react_buffer.back(), c)) {
            react_buffer.pop_back();
        } else {
            react_buffer.push_back(c);
        }
    }

    return react_buffer.size();
}

int main() {
    std::string input{};
    std::string react_buffer{};

    std::getline(std::cin, input);
    react_buffer.reserve(input.size());

    pout("PART1: {}", react(input, react_buffer));

    size_t result{std::numeric_limits<size_t>::max()};
    for (char filter = 'a'; filter <= 'z'; filter++)
        result = std::min(result, react(input, react_buffer, filter));
    pout("PART2: {}", result);

    return 0;
}
