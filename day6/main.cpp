#include <vector>
#include <array>
#include <regex>
#include <optional>
#include <string>
#include <iostream>
#include <fmt/format.h>

#define pout(format, ...) fmt::print(::stdout, format "\n", ##__VA_ARGS__)
#define perr(format, ...) fmt::print(::stderr, format "\n", ##__VA_ARGS__)

namespace fmt {
    template <typename T>
    struct formatter<std::vector<T>> {
        template <typename ParseContext>
        constexpr auto parse(ParseContext &ctx) { return ctx.begin(); }

        template <typename FormatContext>
        auto format(const std::vector<T> &v, FormatContext &ctx) {
            auto ret = format_to(ctx.begin(), "[");
            for (size_t i = 0; i < v.size(); i++)
                ret = format_to(ret, "{}{}", v[i], i < v.size() - 1 ? ", " : "");
            return format_to(ret, "]");
        }
    };
}

struct point {
    int x{0};
    int y{0};

    static inline std::optional<point> parse(const std::string& line) {
        static const std::regex re_point("\\s*(\\d+)\\s*,\\s*(\\d+)\\s*");
        std::smatch m{};

        if (std::regex_match(line, m, re_point))
            return point{std::stoi(m[1].str()), std::stoi(m[2].str())};

        return {};
    }
};
static inline bool operator==(const point& p1, const point& p2) {
    return p1.x == p2.x && p1.y == p2.y;
}

static inline int distance(const point& p1, const point& p2) {
    return std::abs(p1.x - p2.x) + std::abs(p1.y - p2.y);
}

struct rect {
    int x1{0};
    int y1{0};
    int x2{0};
    int y2{0};

    template <typename PointsContainer>
    static inline rect bounding(const PointsContainer& points) {
        rect ret{
            std::numeric_limits<int>::max(), std::numeric_limits<int>::max(),
            std::numeric_limits<int>::min(), std::numeric_limits<int>::min()
        };
        for (const auto& p : points) {
            ret.x1 = std::min(ret.x1, p.x);
            ret.x2 = std::max(ret.x2, p.x);
            ret.y1 = std::min(ret.y1, p.y);
            ret.y2 = std::max(ret.y2, p.y);
        }
        return ret;
    }

    template <typename Predicate>
    inline void for_each_point(const Predicate& pred) const {
        for (int y = y1; y <= y2; y++) {
            for (int x = x1; x <= x2; x++) {
                pred(point{x, y});
            }
        }
    }

    inline bool is_bounding_point(const point& p) const {
        return x1 == p.x || x2 == p.x || y1 == p.y || y2 == p.y;
    }

    inline int width() const {
        return x2 - x1 + 1;
    }
    inline int height() const {
        return y2 - y1 + 1;
    }
};

namespace fmt {
    template <>
    struct formatter<point> {
        template <typename ParseContext>
        constexpr auto parse(ParseContext &ctx) { return ctx.begin(); }

        template <typename FormatContext>
        auto format(const point& p, FormatContext &ctx) {
            return format_to(ctx.begin(), "{{{}, {}}}", p.x, p.y);
        }
    };
    template <>
    struct formatter<rect> {
        template <typename ParseContext>
        constexpr auto parse(ParseContext &ctx) { return ctx.begin(); }

        template <typename FormatContext>
        auto format(const rect& r, FormatContext &ctx) {
            return format_to(ctx.begin(), "{{{{{}, {}}}, {{{}, {}}}}}",
                             r.x1, r.y1, r.x2, r.y2);
        }
    };
}

static std::vector<point> points{};
static rect bounds{};

static inline void part1() {
    std::vector<int> areas(points.size(), 0);
    std::vector<std::vector<size_t>> board(bounds.height(), std::vector<size_t>(bounds.width(), points.size()));

    bounds.for_each_point([&](const point& p) {
        int min_distance{std::numeric_limits<int>::max()};
        size_t min_idx{areas.size()};
        int count{0};
        for (const auto& input_point : points) {
            size_t idx = &input_point - points.data();
            int dist = distance(input_point, p);
            if (dist < min_distance) {
                min_idx = idx;
                min_distance = dist;
                count = 1;
            } else if (dist == min_distance) {
                count += 1;
            }
        }

        if (min_idx < areas.size() && count == 1) {
            areas[min_idx] += 1;
            board[p.y - bounds.y1][p.x - bounds.x1] = min_idx;
        }
    });
    for (const auto& input_point : points) {
        size_t idx = &input_point - points.data();
        if (bounds.is_bounding_point(input_point)) {
            areas[idx] = 0;
            continue;
        }

        for (const auto& maybe_edge : points) {
            if (input_point == maybe_edge) continue;
            if (!bounds.is_bounding_point(maybe_edge)) continue;

            point p1{bounds.x1, input_point.y};
            point p2{bounds.x2, input_point.y};
            point p3{input_point.x, bounds.y1};
            point p4{input_point.x, bounds.y2};
            if (board[p1.y - bounds.y1][p1.x - bounds.x1] == idx ||
                board[p2.y - bounds.y1][p2.x - bounds.x1] == idx ||
                board[p3.y - bounds.y1][p3.x - bounds.x1] == idx ||
                board[p4.y - bounds.y1][p4.x - bounds.x1] == idx) {
                areas[idx] = 0;
                break;
            }
        }
    }

    auto ret = std::max_element(areas.begin(), areas.end());
    assert(ret != areas.end());

#if !defined(NDEBUG)
    pout("AREAS: {}", areas);
    if (areas.size() <= 'z' - 'A' + 1) {
        for (const auto& row : board) {
            std::string s{};
            s.reserve(row.size());
            for (auto idx : row) {
                s.push_back(idx >= points.size() ? '.' : 'A' + idx);
            }
            pout("{}", s);
        }
    }
#endif

    pout("PART 1: {}", *ret);
}

static inline void part2() {
    int result{0};

    bounds.for_each_point([&](const point& p) {
        int sum{0};
        for (const auto& p_in : points)
            sum += distance(p_in, p);
        if (sum < (points.size() == 6 ? 32 : 10000))
            result += 1;
    });

    pout("PART 2: {}", result);
}

int main() {
    std::string line{};

    points.reserve(128);
    while (std::getline(std::cin, line)) {
        auto r = point::parse(line);
        if (!r.has_value()) {
            perr("Failed to parse line '{}'", line);
            return 1;
        }
        points.push_back(r.value());
    }
    bounds = rect::bounding(points);

    part1();
    part2();

    return 0;
}
