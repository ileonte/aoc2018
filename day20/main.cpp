#include <set>
#include <vector>
#include <array>
#include <list>
#include <regex>
#include <optional>
#include <string>
#include <iostream>
#include <fstream>
#include <fmt/format.h>
#include <fmt/ostream.h>

#define pout(format, ...) fmt::print(::stdout, format "\n", ##__VA_ARGS__)
#define perr(format, ...) fmt::print(::stderr, format "\n", ##__VA_ARGS__)
#if defined(NDEBUG)
#define pdbg(format, ...)
#else
#define pdbg(format, ...) pout(format, ##__VA_ARGS__)
#endif

namespace fmt {
    template <typename T>
    struct formatter<std::vector<T>> {
        template <typename ParseContext>
        constexpr auto parse(ParseContext &ctx) { return ctx.begin(); }

        template <typename FormatContext>
        auto format(const std::vector<T> &v, FormatContext &ctx) {
            auto ret = format_to(ctx.begin(), "[");
            for (size_t i = 0; i < v.size(); i++)
                ret = format_to(ret, "{}{}", v[i], i < v.size() - 1 ? ", " : "");
            return format_to(ret, "]");
        }
    };

    template <typename T, size_t N>
    struct formatter<std::array<T, N>> {
        template <typename ParseContext>
        constexpr auto parse(ParseContext& ctx) { return ctx.begin(); }

        template <typename FormatContext>
        auto format(const std::array<T, N>& arr, FormatContext& ctx) {
            auto ret = format_to(ctx.begin(), "[");
            for (size_t i = 0; i < arr.size(); i++)
                ret = format_to(ret, "{}{}", arr[i], i < arr.size() - 1 ? ", " : "");
            return format_to(ret, "]");
        }
    };
}

struct room_t {
    int x{0};
    int y{0};
};
inline bool operator<(const room_t& r1, const room_t& r2) {
    if (r1.y < r2.y) return true;
    if (r1.y == r2.y) return (r1.x < r2.x);
    return false;
}
static std::vector<room_t> rooms{{}};

enum class node_type {
    text,
    open,
    close,
};

struct node {
    std::vector<node*> children{};
    node* parent{nullptr};

    virtual ~node() {
        for (auto* n : children) {
            if (n) delete n;
        }
    }

    virtual node_type type() const = 0;
};

struct text_node : node {
    virtual ~text_node() {}

    inline node_type type() const override final {
        return node_type::text;
    }
};

static inline const char* find_balancing_close_paren(const char* begin, const char* end) {
    assert(*begin == '(');
    auto it = begin;
    int level = 0;
    while (it < end) {
        switch (*it) {
            case '(': {
                level++;
                break;
            }
            case ')': {
                level--;
                if (!level) return it;
                break;
            }
        }
        it++;
    }
    return end;
}

static inline std::vector<std::string_view> regex_split(const char* p_begin, const char* end) {
    std::vector<std::string_view> ret{};
    auto begin = p_begin;
    while (begin < end) {
        const char* it = std::find(begin, end, '(');
        if (it == end) break;

        if (it != begin) ret.push_back(std::string_view(begin, it - begin));
        begin = it;
        it = find_balancing_close_paren(begin, end);
        if (it == end) {
            perr("Unbalanced '(' in input at position '{}'", begin - p_begin);
            std::exit(1);
        }
        auto inner = std::string_view(begin + 1, it - begin - 1);
        regex_split(inner.data(), inner.data() + inner.size());
        ret.push_back(inner);
        begin = it + 1;
    }
    if (begin < end) ret.push_back(std::string_view(begin, end - begin));
    pout("{}", ret);
    return ret;
}

static inline bool read_input(std::istream& in) {
    std::string line{};
    if (!std::getline(in, line)) {
        perr("Error: expecting at least one line of input");
        return false;
    }
    std::string_view line_view(line);
    if (line_view.front() == '^') line_view.remove_prefix(1);
    if (line_view.back() == '$') line_view.remove_suffix(1);
    regex_split(line_view.data(), line_view.data() + line_view.size());
    return false;
}

static inline void part1() {
}

static inline void part2() {
}

int main(int argc, char* argv[]) {
    if (argc >= 2) {
        std::ifstream ifs(argv[1]);
        if (!ifs.is_open()) {
            perr("Failed to open file '{}'", argv[1]);
            return 1;
        }
        if (!read_input(ifs)) return 1;
    } else {
        if (!read_input(std::cin)) return 1;
    }

    part1();
    part2();

    return 0;
}
